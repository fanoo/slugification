<?php
namespace Fanoo\Slugification;

use Ausi\SlugGenerator\SlugGenerator;
Class Slugifier{
    public static function getSlugified(string $string): string
    {
        $generator = new SlugGenerator;
        $slug = $generator->generate($string);

        return $slug;
    }
}