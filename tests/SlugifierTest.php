<?php

use Fanoo\Slugification\Slugifier;
use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertEquals;


Class SlugifierTest extends TestCase{
    public function testSlugification(){

        $this>assertEquals("un-dessin-anime-passe-a-la-tele", Slugifier::getSlugified("Un dessin animé passe à la télé"));
    }
}